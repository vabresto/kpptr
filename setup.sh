# /bin/bash
# Exit on any command returning non-zero error code
set -e

# Keep track of command we're executing
trap 'LAST_CMD=$CURRENT_CMD; CURRENT_CMD=$BASH_COMMAND' DEBUG
# Print a more useful error message when exiting
trap 'echo "\"${LAST_CMD}\" command terminated with exit code $?."' EXIT

git clone https://git.uwaterloo.ca/vabresto/kpptr.git ~/.kpptr

# Remove old symlink, if it exists
if [ -f ~/cs350-os161/os161-1.99/os ]
then
    rm ~/cs350-os161/os161-1.99/os
fi

ln -sfn ~/.kpptr/main.py ~/cs350-os161/os161-1.99/os
chmod u+x ~/cs350-os161/os161-1.99/os
