# Kernel Puppeteer (kpptr)

Welcome to Kernel Puppeteer - a Python program for puppeteering OS 161 and the kernel used in the CS 350 at the University of Waterloo.

# Install Instructions

The simplest way to install kpptr is to use the install script. Simply open a terminal and run
```bash
wget -O - https://git.uwaterloo.ca/vabresto/kpptr/-/raw/master/setup.sh | bash
```

It will download the repo to a temp folder, then extract it to `~/.kpptr` and create a symlink. By default, it will create a symlink called `os` in your 
`~/cs350-os161/os161-1.99` folder, so you can call the program via `./os`. If you want to change this, simply use a soft symlink to 
`~/.kpptr/kpptr-master/main.py`

# Usage Instructions

See `./os --help` for more help.

# Updating
Recent versions of Kernel Puppeteer can check if there are updates available by running
```bash
./os --check-updates
```
and then update by running
```bash
./os --update
```

# Submitting

You can use Kernel Puppeteer to submit your assignment if you're on the student Linux environment by running 
```bash
./os --submit <VERSION>  # or ./os -s <VERSION>
```
where `<VERSION>` is one of
```
1
2a
2b
3
```
for example: `./os --submit 2a`. 

It will run all necessary code, and then hash your submission and keep a record of it. By default, this record is under
```
~/.kpptr/submissions/<VERSION>/
```
The folder will contain a `history.log` file containing the exact timestamps and hashes of what you submitted, as well as the submissions themselves, named by hash.

# Announcements

## More Obvious Errors

Sometimes it is difficult to scan a lot of output to see if all your tests passed. To fix this, all lines with the `[OS]` tag now also include a status tag. The options are as follows:
```
[ ] - No errors; just info
[~] - Warning (probably a failure, but Kernel Puppeteer can't be sure)
[X] - Failure
```

## A3 Tests Available (08/29/2020)

I've updated Kernel Puppeteer with tests for A3. As before, you can choose which tests you want to run.

A3 test command can be run with
```bash
./os --test-virtual-memory  # or ./os -tvm
```
You can specify to run one or more of the following tests:
```
__ALL__ (default)
vm-data1
vm-data3
romemwrite
vm-crash2
sort
matmult
vm-data1-rep
vm-hogparty
vm-widefork
```

The tests that run multiple processes before quitting (`sort`, `matmult`, `vm-data1-rep`, `vm-hogparty`, and `vm-widefork`) each run the specified process exactly 15 times. 

**NOTE:** I've tried to make the memory bounds tight and the time bounds reasonable - I've been able to fit inside the bounds without hitting them and
have run each test at least a dozen times. 

**NOTE:** Per the tests being run, this doesn't check that you've completed your page table implementation.

**NOTE:** There's no "randomness" to these tests, so in theory your code should be freeing and re-using the exact same memory blocks for all of the repeated processes. I'd like
to add some extra tests for this but may not have time.

## A2B Tests Available (08/07/2020)

I've updated Kernel Puppeteer with tests for A2B. Similarly to A2A, you can select which tests you want to run.

A2B test command can be run with
```bash
./os --test-execv  # or ./os -tx
```
You can specify to run one or more of the following tests:
```
__ALL__ (default)
__NO_ARG_PASSING__ (will only run hogparty and sty)
hogparty
sty
argtesttest
argtest
uw-argtest
add
```
The tests should all check for timeouts in your code, so there shouldn't be any issues of complete deadlock.

**NOTE:** I've also included the test case from a Piazza post for `argtest` as well as added a case with lots of arguments to verify correct stack setup.

~~**NOTE:** The `argtest` uses `testbin/argtest` and NOT `uw-testbin/argtest`. Not sure if there's any differences, and I'll try to add the `uw-testbin` version as well later.~~ I've added `uw-argtest`. It *should* be pretty much identical.

**NOTE:** The `add` test runs 55 combinations of inputs, and takes a few seconds to run. Do not be alarmed if it looks like the script has frozen. If it runs longer than 10-15 seconds though, that's probably an issue.

## Extra Tests

I've added some extra tests for execv. They can be run by
```bash
./os --test-execv-extra # or ./os -txx
```

Currently there is only one test run; you can see the source code here: `user/testbin/badcall/bad_execv.c`. I was able to get the test passing entirely, so I think this is a good one to double check as well.

**NOTE:** The built-in OS161 test uses some magic pointer values to verify your pointer validation. I'm not sure if we have the same boundary conditions as in the test, in particular, I'm not sure if the range `0x40000000` to `0x7fffffff` (inclusive) is allowable or not. Based on the UW tests I could find, it **is** allowed, but this test verifies that it is **not** allowed. I've posted a question about it on Piazza as well.

**NOTE:** Kernel Puppeteer does **not** enforce that you return the correct error codes. The OS 161 test will print "passed" or "FAILURE" for each test case it runs, but Kernel Puppeteer only validates that you successfully execute the entire program, and does not enforce that all of the tests say "passed". I encourage you to run the test manually (ie. run `./os -r -a "p testbin/badcall a" --set-mem 4194304`) if you wish to ensure this criteria. You can see what codes are expected for each test to pass in `user/testbin/badcall/bad_execv.c`. It is not enforced because I don't think the graded UW tests check the codes, so I would consider this an extension.

## A2A Tests Available (23/06/2020)

I have added tests for A2A, you can run them with `./os -tf` or `./os --test-fork`. 

Per request, you can specify to run one or more of the following tests:
```
__ALL__ (default)
forktest
forktest-w (runs forktest with the -w argument)
pidcheck
onefork
widefork
```

I've also found that there's another program in our binaries that uses only tools that we will have implemented. Since this isn't part of the tests we were told would be run, I've put it under a different command, which you can run as `./os -tfx` or `./os --test-fork-extra`. It goes through multiple CPU and memory configurations, which was super useful since I found an issue that I wasn't running into with the other tests. If anyone has found any other binaries that work, let me know.

**NOTE:** This test is a bit more flaky - I often get this error: `dumbvm: Ran out of TLB entries - cannot handle page fault`, and because the printing is async, the test pass conditions sometimes get printed jumbled up.

Example output:
```
./os -tfx
[OS] Running uw-testbin/vm-mix1-fork with 1 CPUs and 524288 memory
[OS] vm_mix1_fork failed - Out of memory
[OS] Running uw-testbin/vm-mix1-fork with 2 CPUs and 524288 memory
[OS] vm_mix1_fork failed - Out of memory
[OS] Running uw-testbin/vm-mix1-fork with 4 CPUs and 524288 memory
[OS] vm_mix1_fork failed - Out of memory
[OS] Running uw-testbin/vm-mix1-fork with 8 CPUs and 524288 memory
[OS] vm_mix1_fork failed - Out of memory
[OS] Running uw-testbin/vm-mix1-fork with 1 CPUs and 4194304 memory
[OS] vm_mix1_fork failed - Unable to fork
[OS] Running uw-testbin/vm-mix1-fork with 2 CPUs and 4194304 memory
[OS] vm_mix1_fork failed - Unable to fork
[OS] Running uw-testbin/vm-mix1-fork with 4 CPUs and 4194304 memory
[OS] vm_mix1_fork failed - Unable to fork
[OS] Running uw-testbin/vm-mix1-fork with 8 CPUs and 4194304 memory
[OS] vm_mix1_fork failed - Unable to fork
[OS] Running uw-testbin/vm-mix1-fork with 1 CPUs and 16777216 memory
[OS] vm_mix1_fork PASSED
[OS] Running uw-testbin/vm-mix1-fork with 2 CPUs and 16777216 memory
[OS] vm_mix1_fork PASSED
[OS] Running uw-testbin/vm-mix1-fork with 4 CPUs and 16777216 memory
[OS] vm_mix1_fork PASSED
[OS] Running uw-testbin/vm-mix1-fork with 8 CPUs and 16777216 memory
[OS] vm_mix1_fork PASSED
```

# Example Usage

## Compile ASST1 and run Locks Test

```bash
./os -c 1 -tl # or ./os --compile 1 --test-locks
```

## Start Traffic simulation and wait for debugger

Kernel:
```bash
./os -w -a "sp3" # or ./os --wait --arg "sp3"
```

Debugger:
```bash
./os -d 1 -a "b traffic_synch.c:intersection_sync_init" -a "n"
```
This will break on the init (set breakpoint in gdb), and go to the next line (`n` from gdb console).

# Example Output

## Traffic Test

```
$ ./os -tt
[OS] Traffic Test - Using 5 CPUs in the pool with 1 CPUs in OS 161
[OS] sp3 sanity check succeeded in 0.797482377 seconds
[OS] sp3 passed 100 tests in 19.056804033 seconds with stats: {min: 0.665031977,  avg: 0.7806208861499999, max: 0.900308617}
[OS] sp3 finished with 29 fails and 4 warnings in 100 loops with stats [N: {cars: 1289.0, avg: 0.020982156710628398, max: 0.102}; E: {cars: 1226.0, avg: 0.021486949429037516, max: 0.092}; S: {cars: 1234.0, avg: 0.021606158833063206, max: 0.092}; W: {cars: 1251.0, avg: 0.02217186250999201, max: 0.11} ]
[OS][STATS] Max wait seen for one direction was 0.11 and imbalance of average wait times was 0.0011897057993636127
[OS] Traffic Test - Using 5 CPUs in the pool with 2 CPUs in OS 161
[OS] sp3 sanity check succeeded in 0.771810057 seconds
[OS] sp3 passed 100 tests in 18.446621773 seconds with stats: {min: 0.666153497,  avg: 0.7586099799199999, max: 0.875042737}
[OS] sp3 finished with 30 fails and 3 warnings in 100 loops with stats [N: {cars: 1304.0, avg: 0.021067484662576685, max: 0.101}; E: {cars: 1236.0, avg: 0.022756472491909385, max: 0.111}; S: {cars: 1245.0, avg: 0.021029718875502007, max: 0.101}; W: {cars: 1215.0, avg: 0.0223358024691358, max: 0.101} ]
[OS][STATS] Max wait seen for one direction was 0.111 and imbalance of average wait times was 0.0017267536164073775
[OS] Traffic Test - Using 5 CPUs in the pool with 4 CPUs in OS 161
[OS] sp3 sanity check succeeded in 0.775857377 seconds
[OS] sp3 passed 100 tests in 19.049651696 seconds with stats: {min: 0.641051337,  avg: 0.7493280318599999, max: 0.844893377}
[OS] sp3 finished with 34 fails and 1 warnings in 100 loops with stats [N: {cars: 1172.0, avg: 0.02212713310580205, max: 0.102}; E: {cars: 1323.0, avg: 0.02188284202569917, max: 0.101}; S: {cars: 1223.0, avg: 0.022668029435813575, max: 0.121}; W: {cars: 1282.0, avg: 0.02056786271450858, max: 0.101} ]
[OS][STATS] Max wait seen for one direction was 0.121 and imbalance of average wait times was 0.0021001667213049954
[OS] Traffic Test - Using 5 CPUs in the pool with 8 CPUs in OS 161
[OS] sp3 sanity check succeeded in 0.703507817 seconds
[OS] sp3 passed 100 tests in 19.851153577 seconds with stats: {min: 0.643603137,  avg: 0.72795827455, max: 0.859748337}
[OS] sp3 finished with 38 fails and 0 warnings in 100 loops with stats [N: {cars: 1262.0, avg: 0.02126862123613312, max: 0.101}; E: {cars: 1268.0, avg: 0.021862776025236594, max: 0.101}; S: {cars: 1243.0, avg: 0.021248592115848754, max: 0.092}; W: {cars: 1227.0, avg: 0.021127139364303178, max: 0.111} ]
[OS][STATS] Max wait seen for one direction was 0.111 and imbalance of average wait times was 0.000735636660933417
[OS] *** TRAFFIC TESTS SUCCEEDED IN 80.15813037 SECONDS ***
```

# Contributing

Contributions are welcome! Please create a pull request.
