from subprocess import run

from kpptr.settings import KPPTR_HOME

def check_for_updates():
    _ = run(['git', 'remote', 'update'], cwd=KPPTR_HOME, capture_output=True)
    status = run(['git', 'status', '-uno'], cwd=KPPTR_HOME, capture_output=True)
    status_decoded = status.stdout.decode('utf-8')
    if 'Your branch is up to date with \'origin/master\'' in status_decoded:
        print('[OS][ ] Kernel Puppeteer is up to date.')
    else:
        print('[OS][ ] Update available!')

def run_update():
    update = run(['git', 'pull'], cwd=KPPTR_HOME, capture_output=True)
    update_decoded = update.stdout.decode('utf-8')
    if 'Already up to date.' in update_decoded:
        print('[OS][ ] Kernel Puppeteer is already up to date.')
    elif 'Fast-forward' in update_decoded:
        print('[OS][ ] Successfully updated Kernel Puppeteer!')
    else:
        print(f'[OS][X] Update did not complete successfully ... please try to manually git pull from latest master in {KPPTR_HOME}')