#! /usr/bin/python3.7
from math import ceil
from subprocess import run

from kpptr.stats.operation_time_stats import OperationTimeStats
from kpptr import settings
from kpptr.settings import CS350_ROOT
from kpptr.utils import get_operation_time

def loop_sy2(num_loops: float) -> OperationTimeStats:
    sy2_stats = OperationTimeStats()
    for _ in range(ceil(num_loops)):
        sy2_stats.add_stat(float(sy2_test(False)))
    return sy2_stats


def sy2_test(sanity: bool):
    sy2 = run(['sys161', 'kernel', 'sy2;q'],
              cwd=CS350_ROOT, capture_output=True)
    sy2_decoded = sy2.stdout.decode('utf-8')

    if 'Starting lock test...' in sy2_decoded:
        if 'Lock test done.' in sy2_decoded:
            op_time = get_operation_time(sy2_decoded)
            if sanity:
                print('[OS][ ] sy2 sanity check succeeded in '
                      f'{op_time} seconds')
            if float(op_time) < 0.2:
                print(f'[OS][X] sy2 finished much too quickly. Have you implemented locks?')
                if settings.STRICT_TESTS:
                    exit(1)
            return op_time
        else:
            print(f'[OS][X] sy2 sanity check FAILED: {sy2_decoded}')
            exit(1)
