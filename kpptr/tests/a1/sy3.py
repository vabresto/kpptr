#! /usr/bin/python3.7
from math import ceil
from subprocess import run

from kpptr.stats.operation_time_stats import OperationTimeStats
from kpptr.settings import CS350_ROOT
from kpptr.utils import get_operation_time


def loop_sy3(num_loops: float) -> OperationTimeStats:
    sy3_stats = OperationTimeStats()
    for _ in range(ceil(num_loops)):
        sy3_stats.add_stat(float(sy3_test(False)))
    return sy3_stats


def sy3_test(sanity: bool):
    sy3 = run(['sys161', 'kernel', 'sy3;q'],
              cwd=CS350_ROOT, capture_output=True)
    sy3_decoded = sy3.stdout.decode('utf-8')
    if 'Starting CV test...' in sy3_decoded:
        if 'CV test done' in sy3_decoded:
            op_time = get_operation_time(sy3_decoded)
            if sanity:
                print('[OS][ ] sy3 sanity check succeeded in '
                      f'{op_time} seconds')
            return op_time
        else:
            print(f'[OS][X] sy3 sanity check FAILED: {sy3_decoded}')
            exit(1)