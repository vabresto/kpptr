#! /usr/bin/python3.7
from datetime import datetime
from math import ceil, floor
import random
import re
from typing import Optional, Tuple
from subprocess import run

from kpptr.stats.operation_time_stats import OperationTimeStats
from kpptr.stats.traffic_time_stats import TrafficTimeStats
from kpptr import settings
from kpptr.settings import CS350_ROOT
from kpptr.utils import get_operation_time

def loop_sp3(num_loops: float) -> Tuple[OperationTimeStats, int, int, TrafficTimeStats]:
    sp3_stats = OperationTimeStats()
    num_fails = 0
    num_warns = 0
    overall_traffic = TrafficTimeStats()
    for _ in range(ceil(num_loops)):
        op_time, did_fail, did_warn, traffic = sp3_test(False)
        num_fails += 1 if did_fail else 0
        num_warns += 1 if did_warn else 0
        overall_traffic = TrafficTimeStats.merge(overall_traffic, traffic)
        sp3_stats.add_stat(float(op_time))
    return sp3_stats, num_fails, num_warns, overall_traffic

def sp3_test(sanity: bool, seed: Optional[int] = None):
    command = 'sp3 '
    if seed is None:
        command += '5 10 1 2 0'
    elif seed == -1:
        seed = floor(datetime.now().timestamp())

    if seed is not None:
        print(f'[OS][ ] Seed: {seed}')
        random.seed(seed)
        command += f'{random.randrange(2, 11)} {random.randrange(2, 51)} {random.randrange(2, 51)} {random.randrange(2, 51)} {random.randrange(0, 2)}'
        print(command)

    command += ';q'

    sp3 = run(['sys161', 'kernel', command],
              cwd=CS350_ROOT, capture_output=True)
    sp3_decoded = sp3.stdout.decode('utf-8')
    produced_warning = False
    produced_failure = False
    if 'Threads:' in sp3_decoded:
        if 'Simulation:' in sp3_decoded:
            op_time = get_operation_time(sp3_decoded)
            if sanity:
                print('[OS][ ] sp3 sanity check succeeded in '
                      f'{op_time} seconds')

            # Get max allowed wait
            max_allowed_wait = re.search(r'max permitted wait:\s*(\d*\.\d*)\s*seconds', sp3_decoded)
            assert max_allowed_wait is not None
            waited_north_stats = re.search(r'N:\s*(\d*) vehicles, average wait (\d*\.\d*) seconds, max wait (\d*\.\d*) seconds', sp3_decoded)
            assert waited_north_stats is not None
            waited_east_stats = re.search(r'E:\s*(\d*) vehicles, average wait (\d*\.\d*) seconds, max wait (\d*\.\d*) seconds', sp3_decoded)
            assert waited_east_stats is not None
            waited_south_stats = re.search(r'S:\s*(\d*) vehicles, average wait (\d*\.\d*) seconds, max wait (\d*\.\d*) seconds', sp3_decoded)
            assert waited_south_stats is not None
            waited_west_stats = re.search(r'W:\s*(\d*) vehicles, average wait (\d*\.\d*) seconds, max wait (\d*\.\d*) seconds', sp3_decoded)
            assert waited_west_stats is not None
            
            traffic_stats = TrafficTimeStats(_max_allowed=float(max_allowed_wait.group(1)),
                                             _north=TrafficTimeStats.TrafficDirectionStats.from_logs(waited_north_stats),
                                             _east=TrafficTimeStats.TrafficDirectionStats.from_logs(waited_east_stats),
                                             _south=TrafficTimeStats.TrafficDirectionStats.from_logs(waited_south_stats),
                                             _west=TrafficTimeStats.TrafficDirectionStats.from_logs(waited_west_stats),)

            for stat in {waited_north_stats, waited_east_stats, waited_south_stats, waited_west_stats}:
                if float(stat.group(3)) >= float(max_allowed_wait.group(1)):
                    if settings.STRICT_TESTS:
                        print(f'[OS][X][TRAFFIC][INSUFFICIENT] Max wait for direction exceeded max allowed wait of {max_allowed_wait.group(1)}')
                        print(stat.string)
                        exit(1)
                    produced_failure = True
                    break
                elif float(stat.group(3)) >= float(max_allowed_wait.group(1)) * 0.9:
                    if settings.STRICT_TESTS:
                        print(f'[OS][~][TRAFFIC][WARNING] Max wait for direction nearly hit max allowed wait of {max_allowed_wait.group(1)}')
                        print(stat.string)
                    produced_warning = True
                    break

            if seed is not None:
                total_simulation_time = re.search(r'Simulation:\s*(\d*\.\d*)\s*seconds, \d* vehicles', sp3_decoded)
                assert total_simulation_time is not None
                total_simulation_time = float(total_simulation_time.group(1))

                print('')
                print('--- Results ---')
                print('')

                print(f'Stats: {traffic_stats}')
                passed = (traffic_stats._max_allowed > traffic_stats._north._max_wait and
                            traffic_stats._max_allowed > traffic_stats._east._max_wait and
                            traffic_stats._max_allowed > traffic_stats._south._max_wait and
                            traffic_stats._max_allowed > traffic_stats._west._max_wait)

                # https://www.student.cs.uwaterloo.ca/~cs350/S20/assignments/a1-hints.shtml
                sp3, n, k, i, t, b = command.split(';')[0].split(' ')
                n, k, i, t, b = int(n), int(k), int(i), int(t), int(b)
                millisecond = 1 / 1_000
                if 2 <= n <= 5:
                    max_allowed_simulation_time = 10 * n * k * t * millisecond
                elif n >= 6:
                    max_allowed_simulation_time = 50 * k * t * millisecond
                else:
                    max_allowed_simulation_time = None

                # Efficiency
                if max_allowed_simulation_time is not None and max_allowed_simulation_time >= total_simulation_time:
                    passed = True
                    print(f'\t Efficiency passed: total sim took {total_simulation_time:.4f} of {max_allowed_simulation_time:.4f} allowed')
                elif max_allowed_simulation_time is not None:
                    passed = False
                    print(f'\t Efficiency failed: total sim took {total_simulation_time:.4f} of {max_allowed_simulation_time:.4f} allowed')

                # Fairness - avg wait
                per_direction_avg_failed = False
                if b == 0:
                    # Compute per-direction biases
                    avg_avg = sum([traffic_stats._north._avg_wait,
                                    traffic_stats._east._avg_wait,
                                    traffic_stats._south._avg_wait,
                                    traffic_stats._west._avg_wait]) / 4
                    for dir, label in zip([traffic_stats._north,
                                    traffic_stats._east,
                                    traffic_stats._south,
                                    traffic_stats._west],
                                    ['north', 'east', 'south', 'west']):
                        if abs(dir._avg_wait - avg_avg) > t * millisecond:
                            print(f'\t Fairness (avg) failed: {label} took average of {dir._avg_wait:.4f} of {avg_avg:.4f} +/- {t * millisecond:.4f} allowed')
                            per_direction_avg_failed = True

                    if not per_direction_avg_failed:
                        print(f'\t Fairness (avg) passed: all direction averages were within +/- {t * millisecond:.4f} of {avg_avg:.4f}')

                # Fairness - max wait
                max_dir_wait = (10 * (n-1) * t + 5) * millisecond
                per_direction_max_failed = False
                for dir, label in zip([traffic_stats._north,
                                    traffic_stats._east,
                                    traffic_stats._south,
                                    traffic_stats._west],
                                    ['north', 'east', 'south', 'west']):
                    if dir._max_wait > max_dir_wait:
                        print(f'\t Fairness (max) failed: {label} worst case took {dir._max_wait:.4f} of {max_dir_wait:.4f} allowed')
                        per_direction_max_failed = True

                if not per_direction_max_failed:
                    print('\t Fairness (max) passed: all directions (worst ' +
                            str(max(traffic_stats._north._max_wait,
                                    traffic_stats._east._max_wait,
                                    traffic_stats._south._max_wait,
                                    traffic_stats._west._max_wait))
                            + f') took less than {max_dir_wait:.4f} allowed')

                if not per_direction_max_failed and not per_direction_avg_failed and passed:
                    print('\n[OS][ ] *** TEST PASSED ***')
                else:
                    print('\n[OS][X] *** TEST FAILED ***')
            return op_time, produced_failure, produced_warning, traffic_stats
        else:
            print(f'[OS][X] sp3 sanity check FAILED: {sp3_decoded}')
            exit(1)

    print('Uh oh! Session logs:')
    print(sp3_decoded)