#! /usr/bin/python3.7
from multiprocessing import Pool
from time import time_ns

from kpptr.stats.operation_time_stats import OperationTimeStats
from kpptr.tests.a1.sy2 import loop_sy2, sy2_test
from kpptr.tests.a1.uw1 import loop_uw1, uw1_test
from kpptr.settings import NUM_CPUS, NUM_LOOPS, SIM_NUM_CPUS
from kpptr.utils import set_mainboard_cfg

def test_locks() -> None:
    test_locks_start = time_ns()
    for cpus_avail in SIM_NUM_CPUS:
        set_mainboard_cfg(num_cpus=cpus_avail)

        print(
            f'[OS][ ] Locks Tests - Using {NUM_CPUS} CPUs in the pool with {cpus_avail} CPUs in OS 161')

        sy2_test(True)
        uw1_test(True)

        sy2_stats = OperationTimeStats()
        sy2_start = time_ns()
        with Pool(NUM_CPUS) as pool:
            for stat in pool.imap_unordered(loop_sy2, [NUM_LOOPS / NUM_CPUS] * NUM_CPUS):
                sy2_stats = OperationTimeStats.merge(sy2_stats, stat)
        sy2_end = time_ns()

        print(f'[OS][ ] sy2 passed {sy2_stats._num_data} tests in '
              f'{(sy2_end - sy2_start) / 1_000_000_000 } seconds '
              f'with stats: {sy2_stats}')

        uw1_stats = OperationTimeStats()
        uw1_start = time_ns()
        with Pool(NUM_CPUS) as pool:
            for stat in pool.imap_unordered(loop_uw1, [NUM_LOOPS / NUM_CPUS] * NUM_CPUS):
                uw1_stats = OperationTimeStats.merge(uw1_stats, stat)
        uw1_end = time_ns()

        print(f'[OS][ ] uw1 passed {uw1_stats._num_data} tests in '
              f'{(uw1_end - uw1_start) / 1_000_000_000 } seconds '
              f'with stats: {uw1_stats}')

    test_locks_end = time_ns()
    print(
        f'[OS][ ] *** LOCK TESTS SUCCEEDED IN { (test_locks_end - test_locks_start) / 1_000_000_000 } SECONDS ***')