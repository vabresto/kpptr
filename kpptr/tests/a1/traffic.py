#! /usr/bin/python3.7
from multiprocessing import Pool
from time import time_ns

from kpptr.stats.operation_time_stats import OperationTimeStats
from kpptr.stats.traffic_time_stats import TrafficTimeStats
from kpptr.tests.a1.sp3 import loop_sp3, sp3_test
from kpptr.settings import NUM_CPUS, NUM_LOOPS, SIM_NUM_CPUS
from kpptr.utils import set_mainboard_cfg

def test_traffic() -> None:
    test_traffic_start = time_ns()
    for cpus_avail in SIM_NUM_CPUS:
        set_mainboard_cfg(num_cpus=cpus_avail)

        print(
            f'[OS][ ] Traffic Test - Using {NUM_CPUS} CPUs in the pool with {cpus_avail} CPUs in OS 161')

        sp3_test(True)

        sp3_stats = OperationTimeStats()
        total_num_fails = 0
        total_num_warns = 0
        total_traffic = TrafficTimeStats()
        sp3_start = time_ns()
        with Pool(NUM_CPUS) as pool:
            for res in pool.imap_unordered(loop_sp3, [NUM_LOOPS / NUM_CPUS] * NUM_CPUS):
                stat, fails, warns, traffic = res
                total_num_fails += fails
                total_num_warns += warns
                total_traffic = TrafficTimeStats.merge(total_traffic, traffic)
                sp3_stats = OperationTimeStats.merge(sp3_stats, stat)
        sp3_end = time_ns()

        print(f'[OS][ ] sp3 passed {sp3_stats._num_data} tests in '
              f'{(sp3_end - sp3_start) / 1_000_000_000 } seconds '
              f'with stats: {sp3_stats}')

        if total_num_fails > 0 or total_num_warns > 0:
            print(f'[OS][ ] sp3 finished with {total_num_fails} fails and {total_num_warns} warnings in {NUM_LOOPS} loops with stats {total_traffic}')
            print(f'[OS][ ][STATS] Max wait seen for one direction was {total_traffic.max_wait_incurred()} and imbalance of average wait times was {total_traffic.imbalance_of_averages()}')

    test_traffic_end = time_ns()
    print(
        f'[OS][ ] *** TRAFFIC TESTS SUCCEEDED IN { (test_traffic_end - test_traffic_start) / 1_000_000_000 } SECONDS ***')
