#! /usr/bin/python3.7
from multiprocessing import Pool
from time import time_ns

from kpptr.stats.operation_time_stats import OperationTimeStats
from kpptr.tests.a1.sy3 import loop_sy3, sy3_test
from kpptr.settings import NUM_CPUS, NUM_LOOPS, SIM_NUM_CPUS
from kpptr.utils import set_mainboard_cfg

def test_cvs() -> None:
    test_cvs_start = time_ns()
    for cpus_avail in SIM_NUM_CPUS:
        set_mainboard_cfg(num_cpus=cpus_avail)

        print(
            f'[OS][ ] CV Test - Using {NUM_CPUS} CPUs in the pool with {cpus_avail} CPUs in OS 161')

        sy3_test(True)

        sy3_stats = OperationTimeStats()
        sy3_start = time_ns()
        with Pool(NUM_CPUS) as pool:
            for stat in pool.imap_unordered(loop_sy3, [NUM_LOOPS / NUM_CPUS] * NUM_CPUS):
                sy3_stats = OperationTimeStats.merge(sy3_stats, stat)
        sy3_end = time_ns()

        print(f'[OS][ ] sy3 passed {sy3_stats._num_data} tests in '
              f'{(sy3_end - sy3_start) / 1_000_000_000 } seconds '
              f'with stats: {sy3_stats}')

    test_cvs_end = time_ns()
    print(
        f'[OS][ ] *** CV TESTS SUCCEEDED IN { (test_cvs_end - test_cvs_start) / 1_000_000_000 } SECONDS ***')