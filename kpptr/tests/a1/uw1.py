#! /usr/bin/python3.7
from math import ceil
from subprocess import run

from kpptr.stats.operation_time_stats import OperationTimeStats
from kpptr.settings import CS350_ROOT
from kpptr.utils import get_operation_time

def loop_uw1(num_loops: float) -> OperationTimeStats:
    uw1_stats = OperationTimeStats()
    for _ in range(ceil(num_loops)):
        uw1_stats.add_stat(float(uw1_test(False)))
    return uw1_stats


def uw1_test(sanity: bool):
    uw1 = run(['sys161', 'kernel', 'uw1;q'],
              cwd=CS350_ROOT, capture_output=True)
    uw1_decoded = uw1.stdout.decode('utf-8')
    if 'Starting uwlocktest1...' in uw1_decoded:
        if 'TEST SUCCEEDED' in uw1_decoded:
            op_time = get_operation_time(uw1_decoded)
            if sanity:
                print('[OS][ ] uw1 sanity check succeeded in '
                      f'{op_time} seconds')
            return op_time
        else:
            print(f'[OS][X] uw1 sanity check FAILED: {uw1_decoded}')
            exit(1)