from argparse import Namespace
from datetime import datetime
import math

from kpptr.tests.a1.cvs import test_cvs
from kpptr.tests.a1.sp3 import sp3_test
from kpptr.tests.a1.locks import test_locks
from kpptr.tests.a1.traffic import test_traffic
from kpptr.tests.a2.fork import test_all_fork_tests
from kpptr.tests.a2.fork_extra import test_vm_mix1_fork
from kpptr.tests.a2.execv import test_all_execv_tests
from kpptr.tests.a2.execv_extra import test_all_execv_extra_tests
from kpptr.tests.a3.vm import test_all_vm_tests
from kpptr.utils import set_mainboard_cfg

def check_and_run_tests(args: Namespace) -> None:
    if args.test_locks is not None and args.test_locks:
        test_locks()
        set_mainboard_cfg(num_cpus=1)

    if args.test_cvs is not None and args.test_cvs:
        test_cvs()
        set_mainboard_cfg(num_cpus=1)

    if args.test_traffic is not None and args.test_traffic:
        test_traffic()
        set_mainboard_cfg(num_cpus=1)

    if args.test_traffic_random is not None:
        seed = math.floor(datetime.now().timestamp())
        if args.test_traffic_random != -1:
            seed = args.test_traffic_random
        sp3_test(False, seed)
        set_mainboard_cfg(num_cpus=1)

    if args.test_fork is not None:
        test_all_fork_tests(args.test_fork)

    if args.test_fork_extra is not None and args.test_fork_extra:
        test_vm_mix1_fork()

    if args.test_execv is not None:
        test_all_execv_tests(args.test_execv)

    if args.test_execv_extra is not None:
        test_all_execv_extra_tests(args.test_execv_extra)

    if args.test_virtual_memory is not None:
        test_all_vm_tests(args.test_virtual_memory)
