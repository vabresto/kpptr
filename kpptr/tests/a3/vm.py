from collections import defaultdict
from multiprocessing import Pool
import re
from subprocess import run, TimeoutExpired
from time import time_ns
from typing import List

from kpptr.stats.operation_time_stats import OperationTimeStats
from kpptr import settings
from kpptr.settings import CS350_ROOT, NUM_CPUS, NUM_LOOPS, SIM_NUM_CPUS
from kpptr.utils import set_mainboard_cfg

all_tests = '__ALL__'

def test_vm_data_simple(vm_prog):
    assert vm_prog in {1, 3}
    try:
        set_mainboard_cfg(num_cpus=1, ram_size=2347008)
        vm_data = run(['sys161', 'kernel', f'p uw-testbin/vm-data{vm_prog};q'],
                cwd=CS350_ROOT, capture_output=True, timeout=5)
        vm_data_decoded = vm_data.stdout.decode('utf-8')
        output_str = re.search(r'.*p uw-testbin\/vm-data(.*)Operation took.*', vm_data_decoded, re.DOTALL)
        assert output_str is not None
        output_str = output_str.group(1)

        if 'SUCCEEDED' in output_str:
            print(f'[OS][ ] vm-data{vm_prog} PASSED')
        else:
            print (f'[OS][X] vm-data{vm_prog} FAILED')
            if settings.STRICT_TESTS:
                exit(1)
        
    except TimeoutExpired:
        print(f'[OS][X] vm-data{vm_prog} failed - Timeout exceeded!')
        if settings.STRICT_TESTS:
            exit(1)
    except AssertionError:
        print(vm_data_decoded)
        print(f'[OS][X] vm-data{vm_prog} failed - Program did not execute correctly!')
        if settings.STRICT_TESTS:
            exit(1)

def test_romemwrite():
    try:
        set_mainboard_cfg(num_cpus=1, ram_size=524288)
        romemwrite = run(['sys161', 'kernel', f'p uw-testbin/romemwrite;q'],
                cwd=CS350_ROOT, capture_output=True, timeout=5)
        romemwrite_decoded = romemwrite.stdout.decode('utf-8')
        output_str = re.search(r'.*p uw-testbin\/romemwrite(.*)Operation took.*', romemwrite_decoded, re.DOTALL)
        assert output_str is not None
        output_str = output_str.group(1)

        if 'FAILED' not in output_str:
            print(f'[OS][ ] romemwrite PASSED')
        else:
            print (f'[OS][X] romemwrite FAILED')
            if settings.STRICT_TESTS:
                exit(1)
        
    except TimeoutExpired:
        print(f'[OS][X] romemwrite failed - Timeout exceeded!')
        if settings.STRICT_TESTS:
            exit(1)
    except AssertionError:
        print(romemwrite_decoded)
        print(f'[OS][X] romemwrite failed - Program did not execute correctly!')
        if settings.STRICT_TESTS:
            exit(1)


def test_vm_crash2():
    try:
        set_mainboard_cfg(num_cpus=1, ram_size=524288)
        vm_crash2 = run(['sys161', 'kernel', f'p uw-testbin/vm-crash2;q'],
                cwd=CS350_ROOT, capture_output=True, timeout=5)
        vm_crash2_decoded = vm_crash2.stdout.decode('utf-8')
        output_str = re.search(r'.*p uw-testbin\/vm-crash2(.*)Operation took.*', vm_crash2_decoded, re.DOTALL)
        assert output_str is not None
        output_str = output_str.group(1)

        if 'FAILED' not in output_str:
            print(f'[OS][ ] vm-crash2 PASSED')
        else:
            print (f'[OS][X] vm-crash2 FAILED')
            if settings.STRICT_TESTS:
                exit(1)
        
    except TimeoutExpired:
        print(f'[OS][X] vm-crash2 failed - Timeout exceeded!')
        if settings.STRICT_TESTS:
            exit(1)
    except AssertionError:
        print(vm_crash2_decoded)
        print(f'[OS][X] vm-crash2 failed - Program did not execute correctly!')
        if settings.STRICT_TESTS:
            exit(1)

def test_vm_sort():
    print(f'[OS][ ] Running sort (may take up to 65 seconds to complete) ... ', end='', flush=True)
    num_reps = 15
    try:
        set_mainboard_cfg(num_cpus=1, ram_size=1527808)
        vm_sort = run(['sys161', 'kernel'] + [f'p testbin/sort;'] * num_reps + ['q'],
                cwd=CS350_ROOT, capture_output=True, timeout=65)
        vm_sort_decoded = vm_sort.stdout.decode('utf-8')
        print('DONE')

        if vm_sort_decoded.count('testbin/sort: Passed.') == num_reps:
            print(f'[OS][ ] sort PASSED')
        else:
            print (f'[OS][X] sort FAILED')
            if settings.STRICT_TESTS:
                exit(1)
        
    except TimeoutExpired:
        print('DONE')
        print(f'[OS][X] sort failed - Timeout exceeded!')
        if settings.STRICT_TESTS:
            exit(1)
    except AssertionError:
        print('DONE')
        print(vm_sort_decoded)
        print(f'[OS][X] sort failed - Program did not execute correctly!')
        if settings.STRICT_TESTS:
            exit(1)

def test_vm_matmult():
    print(f'[OS][ ] Running matmult (may take up to 20 seconds to complete) ... ', end='', flush=True)
    num_reps = 15
    try:
        set_mainboard_cfg(num_cpus=1, ram_size=1916928)
        vm_matmult = run(['sys161', 'kernel'] + [f'p testbin/matmult;'] * num_reps + ['q'],
                cwd=CS350_ROOT, capture_output=True, timeout=20)
        vm_matmult_decoded = vm_matmult.stdout.decode('utf-8')
        print('DONE')

        if vm_matmult_decoded.count('Passed.') == num_reps:
            print(f'[OS][ ] matmult PASSED')
        else:
            print (f'[OS][X] matmult FAILED')
            print(vm_matmult_decoded)
            if settings.STRICT_TESTS:
                exit(1)
        
    except TimeoutExpired:
        print('DONE')
        print(f'[OS][X] matmult failed - Timeout exceeded!')
        if settings.STRICT_TESTS:
            exit(1)
    except AssertionError:
        print('DONE')
        print(vm_matmult_decoded)
        print(f'[OS][X] matmult failed - Program did not execute correctly!')
        if settings.STRICT_TESTS:
            exit(1)

def test_vm_data1_rep():
    print(f'[OS][ ] Running vm_data1 repeatedly (may take up to 10 seconds to complete) ... ', end='', flush=True)
    num_reps = 15
    try:
        set_mainboard_cfg(num_cpus=1, ram_size=913408)
        vm_data1 = run(['sys161', 'kernel'] + [f'p uw-testbin/vm-data1;'] * num_reps + ['q'],
                cwd=CS350_ROOT, capture_output=True, timeout=10)
        vm_data1_decoded = vm_data1.stdout.decode('utf-8')
        print('DONE')

        if vm_data1_decoded.count('SUCCEEDED') == num_reps:
            print(f'[OS][ ] vm_data1 repeated PASSED')
        else:
            print (f'[OS][X] vm_data1 repeated FAILED')
            if settings.STRICT_TESTS:
                exit(1)
        
    except TimeoutExpired:
        print('DONE')
        print(f'[OS][X] vm_data1 repeated failed - Timeout exceeded!')
        if settings.STRICT_TESTS:
            exit(1)
    except AssertionError:
        print('DONE')
        print(vm_data1_decoded)
        print(f'[OS][X] vm_data1 repeated failed - Program did not execute correctly!')
        if settings.STRICT_TESTS:
            exit(1)

def test_vm_widefork():
    widefork_correct_output = {
        'P': 3,
        'A': 1,
        'B': 1,
        'C': 1,
        'a': 1,
        'b': 1,
        'c': 1,
        'x': 0,
    }

    print(f'[OS][ ] Running vm_widefork (may take up to 10 seconds to complete) ... ', end='', flush=True)
    num_reps = 15
    try:
        set_mainboard_cfg(num_cpus=1, ram_size=1323008)
        vm_widefork = run(['sys161', 'kernel'] + [f'p uw-testbin/widefork;'] * num_reps + ['q'],
                cwd=CS350_ROOT, capture_output=True, timeout=10)
        vm_widefork_decoded = vm_widefork.stdout.decode('utf-8')
        print('DONE')

        raw = vm_widefork_decoded.split('widefork')
        edited = []
        for item in raw:
            sub_items = item.split('Operation')

            if 'took' in sub_items[0]:
                item = sub_items[1]
            elif len(sub_items) > 1 and 'took' in sub_items[1]:
                item = sub_items[0]
            else:
                continue

            item = item.replace("\n", "").replace("\r", "")
            should_add = True
            for letter in 'defghijklmnopqrstuvwyz':
                if letter in item:
                    should_add = False
                    break
            if should_add:
                edited.append(item)

        assert(len(edited) == 15)

        all_correct = True
        for item in edited:
            output = defaultdict(lambda: 0)
            for letter in item:
                output[letter] += 1

            for key in widefork_correct_output.keys():
                if widefork_correct_output[key] != output[key]:
                    all_correct = False
        
        if all_correct:
            print('[OS][ ] vm_widefork PASSED')
        else:
            print(vm_widefork_decoded)
            print (f'[OS][X] vm_widefork repeated FAILED')
            if settings.STRICT_TESTS:
                exit(1)

    except TimeoutExpired:
        print('DONE')
        print(f'[OS][X] vm_widefork failed - Timeout exceeded!')
        if settings.STRICT_TESTS:
            exit(1)
    except AssertionError:
        print('DONE')
        print(vm_widefork_decoded)
        print(f'[OS][X] vm_widefork failed - Program did not execute correctly!')
        if settings.STRICT_TESTS:
            exit(1)

def test_vm_hogparty():
    hogparty_correct_output = {
        'x': 5,
        'y': 5,
        'z': 5,
    }

    print(f'[OS][ ] Running vm_hogparty (may take up to 10 seconds to complete) ... ', end='', flush=True)
    num_reps = 15
    try:
        set_mainboard_cfg(num_cpus=1, ram_size=1323008)
        vm_hogparty = run(['sys161', 'kernel'] + [f'p uw-testbin/hogparty;'] * num_reps + ['q'],
                cwd=CS350_ROOT, capture_output=True, timeout=10)
        vm_hogparty_decoded = vm_hogparty.stdout.decode('utf-8')
        print('DONE')

        raw = vm_hogparty_decoded.split('hogparty')
        edited = []
        for item in raw:
            sub_items = item.split('Operation')

            if 'took' in sub_items[0]:
                item = sub_items[1]
            elif len(sub_items) > 1 and 'took' in sub_items[1]:
                item = sub_items[0]
            else:
                continue

            item = item.replace("\n", "").replace("\r", "")
            should_add = True
            for letter in 'abcdefghijklmnopqrstuvw':
                if letter in item:
                    should_add = False
                    break
            if should_add:
                edited.append(item)

        assert(len(edited) == 15)

        all_correct = True
        for item in edited:
            output = defaultdict(lambda: 0)
            for letter in item:
                output[letter] += 1

            for key in hogparty_correct_output.keys():
                if hogparty_correct_output[key] != output[key]:
                    all_correct = False
        
        if all_correct:
            print('[OS][ ] vm_hogparty PASSED')
        else:
            print(vm_hogparty_decoded)
            print (f'[OS][X] vm_hogparty FAILED')
            if settings.STRICT_TESTS:
                exit(1)

    except TimeoutExpired:
        print('DONE')
        print(f'[OS][X] vm_hogparty failed - Timeout exceeded!')
        if settings.STRICT_TESTS:
            exit(1)
    except AssertionError:
        print('DONE')
        print(vm_hogparty_decoded)
        print(f'[OS][X] vm_hogparty failed - Program did not execute correctly!')
        if settings.STRICT_TESTS:
            exit(1)

def test_all_vm_tests(which_tests: List[str]):
    if which_tests == []:
        which_tests = [all_tests]

    # Check for invalid params
    any_bad = False
    for test in which_tests:
        if test not in {all_tests, 'vm-data1', 'vm-data3', 'romemwrite', 'vm-crash2', 'sort', 'matmult', 'vm-data1-rep', 'vm-hogparty', 'vm-widefork'}:
            print(f'[OS][X] Got bad argument to test vm: {test}')
            any_bad = True

    if any_bad and settings.STRICT_TESTS:
        exit(1)

    set_mainboard_cfg(num_cpus=1)

    if all_tests in which_tests or 'vm-data1' in which_tests:
        test_vm_data_simple(1)
    if all_tests in which_tests or 'vm-data3' in which_tests:
        test_vm_data_simple(3)
    if all_tests in which_tests or 'romemwrite' in which_tests:
        test_romemwrite()
    if all_tests in which_tests or 'vm-crash2' in which_tests:
        test_vm_crash2()
    if all_tests in which_tests or 'sort' in which_tests:
        test_vm_sort()
    if all_tests in which_tests or 'matmult' in which_tests:
        test_vm_matmult()
    if all_tests in which_tests or 'vm-data1-rep' in which_tests:
        test_vm_data1_rep()
    if all_tests in which_tests or 'vm-hogparty' in which_tests:
        test_vm_hogparty()
    if all_tests in which_tests or 'vm-widefork' in which_tests:
        test_vm_widefork()
    

    # Reset to defaults
    set_mainboard_cfg(num_cpus=1, ram_size=524288)