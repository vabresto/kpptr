from collections import defaultdict
import re
from subprocess import run, TimeoutExpired
from time import time_ns
from typing import List

from kpptr.stats.operation_time_stats import OperationTimeStats
from kpptr import settings
from kpptr.settings import CS350_ROOT, NUM_CPUS, NUM_LOOPS, SIM_NUM_CPUS
from kpptr.utils import set_mainboard_cfg

all_tests = '__ALL__'
no_arg_passing_tests = '__NO_ARG_PASSING__'

def test_hogparty():
    hogparty_correct_output = {
        'x': 5,
        'y': 5,
        'z': 5,
    }

    try:
        hogparty = run(['sys161', 'kernel', 'p uw-testbin/hogparty;q'],
                cwd=CS350_ROOT, capture_output=True, timeout=5)
        hogparty_decoded = hogparty.stdout.decode('utf-8')
        output_str = re.search(r'.*p uw-testbin\/hogparty(.*)Operation took.*', hogparty_decoded, re.DOTALL)
        assert output_str is not None
        output_str = output_str.group(1)

        output = defaultdict(lambda: 0)
        for letter in output_str:
            output[letter] += 1

        all_correct = True
        for key in hogparty_correct_output.keys():
            if hogparty_correct_output[key] != output[key]:
                print(f'[OS][ ] hogparty failed - Got {output[key]} of {key} but expected {hogparty_correct_output[key]}')
                all_correct = False

        if all_correct:
            print('[OS][ ] hogparty PASSED')
        elif settings.STRICT_TESTS:
            exit(1)
        
    except TimeoutExpired:
        print('[OS][X] hogparty failed - Timeout exceeded!')
        if settings.STRICT_TESTS:
            exit(1)
    except AssertionError:
        print(hogparty_decoded)
        print('[OS][X] hogparty failed - Program did not execute correctly!')
        if settings.STRICT_TESTS:
            exit(1)

def test_sty():
    try:
        sty = run(['sys161', 'kernel', 'p testbin/sty;q'],
                cwd=CS350_ROOT, capture_output=True, timeout=5)
        sty_decoded = sty.stdout.decode('utf-8')
        output_str = re.search(r'.*p testbin\/sty(.*)Operation took.*', sty_decoded, re.DOTALL)
        assert output_str is not None
        print('[OS][ ] sty PASSED')
        
    except TimeoutExpired:
        print('[OS][X] sty failed - Timeout exceeded!')
        if settings.STRICT_TESTS:
            exit(1)
    except AssertionError:
        print(sty_decoded)
        print('[OS][X] sty failed - Program did not execute correctly!')
        if settings.STRICT_TESTS:
            exit(1)

def test_argtesttest():
    try:
        argtesttest = run(['sys161', 'kernel', 'p uw-testbin/argtesttest;q'],
                cwd=CS350_ROOT, capture_output=True, timeout=5)
        argtesttest_decoded = argtesttest.stdout.decode('utf-8')

        argc_str = re.search(r'.*argc: (\d*).*', argtesttest_decoded)
        assert argc_str is not None
        argc_str = argc_str.group(1)

        argv0_str = re.search(r'.*argv\[0\]: (\S*).*', argtesttest_decoded)
        assert argv0_str is not None
        argv0_str = argv0_str.group(1)

        argv1_str = re.search(r'.*argv\[1\]: (\S*).*', argtesttest_decoded)
        assert argv1_str is not None
        argv1_str = argv1_str.group(1)

        argv2_str = re.search(r'.*argv\[2\]: (\S*).*', argtesttest_decoded)
        assert argv2_str is not None
        argv2_str = argv2_str.group(1)

        argv3_str = re.search(r'.*argv\[3\]: (\S*).*', argtesttest_decoded)
        assert argv3_str is not None
        argv3_str = argv3_str.group(1)

        if int(argc_str) == 3 and argv0_str == 'argtesttest' and argv1_str == 'first' and argv2_str == 'second' and argv3_str == '[NULL]':
            print('[OS][ ] argtesttest PASSED')
        else:
            print(argtesttest_decoded)
            print('[OS][X] argtesttest FAILED')
            if settings.STRICT_TESTS:
                exit(1)
        
    except TimeoutExpired:
        print('[OS][X] argtesttest failed - Timeout exceeded!')
        if settings.STRICT_TESTS:
            exit(1)
    except AssertionError:
        print(argtesttest_decoded)
        print('[OS][X] argtesttest failed - Program did not execute correctly!')
        if settings.STRICT_TESTS:
            exit(1)

def test_argtest():
    argtest_inputs = [
        ['tests'],
        ['hello', 'world'],
        [], # Intentionally empty
        ['La', 'dee', 'dah'],
        ["'bob'", "'rich'", "'ha'", "'wa'", "'da'", "'mu'"],
        [
            'OyokfhegwCGnn8oaEaSiC9fKvEpSOLNQICisNOZqXsabpX1XDDhEgOrfbcA7NKTnHnRr9U6u8UBO4ZFD',
            'XVKkUmPtevZtXPlwhrQaDDYrUovDWXGaEjiEtNR7bsesxgoHITMqyeFq0LTx1EcocShaAi3uYYnfAMtD',
            'STWcyrO1Phu9S7yYkRwmLRiuJVhBZc29kqtfQoHfL5UKLfUJLTdKZzgf894U3fXcCeZDSlUgawO4ku6U',
            'AEOYEBGJzesn7pBhqOU2O7NYBX7wUa1QdoZow3nKwoqFtXxWW7QQWQa9yaXGQd9GK3iT0YI397SXUW4s',
            'GajKS6XQygbi80QAGqYiQ2yhFV8AbLuBOTyClMP6rV5Zs4u2C85LBd2L3hUPBbxUKpSKwFEiwHVEQbeB',
            'ze3EcP9TX3CcCrOquAdou0ktp0seLTHPy1jn4ofurv0ubqv2fxmXqL5MOyHrXKBhmDQuDmwAxn4Rf47k',
            'lG5OAi5MZIX0YBreQz2TYvLittSQEBVtURfNFjPRHFKTNZ8sTjbusEcXy444E6IDk9Unfur3rD8HcHkL',
            'dP5OuDEA0LJVoAaDlpjuE8dos1QlAqyR7gdlcRcsYmnX3jqi452IsFYAe24bZdLbPaKtgPCKCgl7OsFJ',
            'JQ1FlbX4D8ksYdIWIkXD0VARkebj1aINSiFDW5fnPhIXVHfh4EsuHO3CkiHLynH4T2VJy6lxPOLP4OQP',
            'YuPVtyxxK0VtYmp10Yf8wNrLlg05z3DRFQxzqKeSEWNfpH0BeRkrqI0nHjChUT3LfwVQ9t9pJdzOWOPm',
            'mnR7Ma8Tq1SnyFNHgzz12ZD0VlymICCMNKIPK2O9BJoPjk8At7Y2h3g9slRSssoDBnmXDBqL9AhgJon5',
            'lNVVKU8lIsUcblt17Cu59EFuM9tsMNXYCjVrFqoTrzI8gIUgKJNd9TaKNYear1RzTG2Bvo7SAZZ1csUe',
            'HGrS0cxxIpkAKSFUaJE3dkXZTCdaWGmYx0PypEdSKaMVS0ne7euzz5Jz2cDR8z63ja5rMmmWAqrZbpLx',
            'mIY8TqcZMZMk71aZGMN2oUGuA0Mms4x6bVPt3g1CctM2oWfSTkU2CWkxKoMlUgfStIE9A8j7thCwjcwk',
        ],
    ]

    all_runs_correct = True
    for args in argtest_inputs:
        try:
            argtest = run(['sys161', 'kernel', 'p', 'testbin/argtest'] + args + [';q'],
                    cwd=CS350_ROOT, capture_output=True, timeout=5)
            argtest_decoded = argtest.stdout.decode('utf-8')

            all_args_correct = True
            argc_str = re.search(r'.*argc: (\d*).*', argtest_decoded)
            assert argc_str is not None
            argc_str = int(argc_str.group(1))
            if argc_str != len(args) + 1:
                print(f'[OS][X] argtest failed - expected {len(args) + 1} args but got {argc_str}')
                all_args_correct = False

            for num, arg in enumerate(['testbin/argtest'] + args):
                arg_str = re.search(rf'argv\[{num}\]: (\S*).*', argtest_decoded)
                assert arg_str is not None
                arg_str = arg_str.group(1)
                if arg_str != arg:
                    print(f'[OS][X] argtest failed - arg {num} should be {arg} but was {arg_str}')
                    all_args_correct = False

            if not all_args_correct:
                all_runs_correct = False
                print(' '.join(['[OS][X] argtest failed - input was: p testbin/argtest'] + args))
                if settings.STRICT_TESTS:
                    exit(1)
            
        except TimeoutExpired:
            print('[OS][X] argtest failed - Timeout exceeded!')
            if settings.STRICT_TESTS:
                exit(1)
        except AssertionError:
            print(argtest_decoded)
            print('[OS][X] argtest failed - Program did not execute correctly!')
            if settings.STRICT_TESTS:
                exit(1)

    if all_runs_correct:
        print('[OS][ ] argtest PASSED')


def test_uw_argtest():
    argtest_inputs = [
        ['tests'],
        ['hello', 'world'],
        [], # Intentionally empty
        ['La', 'dee', 'dah'],
        ["'bob'", "'rich'", "'ha'", "'wa'", "'da'", "'mu'"],
        [
            'OyokfhegwCGnn8oaEaSiC9fKvEpSOLNQICisNOZqXsabpX1XDDhEgOrfbcA7NKTnHnRr9U6u8UBO4ZFD',
            'XVKkUmPtevZtXPlwhrQaDDYrUovDWXGaEjiEtNR7bsesxgoHITMqyeFq0LTx1EcocShaAi3uYYnfAMtD',
            'STWcyrO1Phu9S7yYkRwmLRiuJVhBZc29kqtfQoHfL5UKLfUJLTdKZzgf894U3fXcCeZDSlUgawO4ku6U',
            'AEOYEBGJzesn7pBhqOU2O7NYBX7wUa1QdoZow3nKwoqFtXxWW7QQWQa9yaXGQd9GK3iT0YI397SXUW4s',
            'GajKS6XQygbi80QAGqYiQ2yhFV8AbLuBOTyClMP6rV5Zs4u2C85LBd2L3hUPBbxUKpSKwFEiwHVEQbeB',
            'ze3EcP9TX3CcCrOquAdou0ktp0seLTHPy1jn4ofurv0ubqv2fxmXqL5MOyHrXKBhmDQuDmwAxn4Rf47k',
            'lG5OAi5MZIX0YBreQz2TYvLittSQEBVtURfNFjPRHFKTNZ8sTjbusEcXy444E6IDk9Unfur3rD8HcHkL',
            'dP5OuDEA0LJVoAaDlpjuE8dos1QlAqyR7gdlcRcsYmnX3jqi452IsFYAe24bZdLbPaKtgPCKCgl7OsFJ',
            'JQ1FlbX4D8ksYdIWIkXD0VARkebj1aINSiFDW5fnPhIXVHfh4EsuHO3CkiHLynH4T2VJy6lxPOLP4OQP',
            'YuPVtyxxK0VtYmp10Yf8wNrLlg05z3DRFQxzqKeSEWNfpH0BeRkrqI0nHjChUT3LfwVQ9t9pJdzOWOPm',
            'mnR7Ma8Tq1SnyFNHgzz12ZD0VlymICCMNKIPK2O9BJoPjk8At7Y2h3g9slRSssoDBnmXDBqL9AhgJon5',
            'lNVVKU8lIsUcblt17Cu59EFuM9tsMNXYCjVrFqoTrzI8gIUgKJNd9TaKNYear1RzTG2Bvo7SAZZ1csUe',
            'HGrS0cxxIpkAKSFUaJE3dkXZTCdaWGmYx0PypEdSKaMVS0ne7euzz5Jz2cDR8z63ja5rMmmWAqrZbpLx',
            'mIY8TqcZMZMk71aZGMN2oUGuA0Mms4x6bVPt3g1CctM2oWfSTkU2CWkxKoMlUgfStIE9A8j7thCwjcwk',
        ],
    ]

    all_runs_correct = True
    for args in argtest_inputs:
        try:
            argtest = run(['sys161', 'kernel', 'p', 'uw-testbin/argtest'] + args + [';q'],
                    cwd=CS350_ROOT, capture_output=True, timeout=5)
            argtest_decoded = argtest.stdout.decode('utf-8')

            all_args_correct = True
            argc_str = re.search(r'.*argc\s*: (\d*).*', argtest_decoded)
            assert argc_str is not None
            argc_str = int(argc_str.group(1))
            if argc_str != len(args) + 1:
                print(f'[OS][X] uw-argtest failed - expected {len(args) + 1} args but got {argc_str}')
                all_args_correct = False

            for num, arg in enumerate(['uw-testbin/argtest'] + args + ['[NULL]']):
                arg_str = re.search(rf'argv\[{num}\] -> (\S*).*', argtest_decoded)
                assert arg_str is not None
                arg_str = arg_str.group(1)
                if arg_str != arg:
                    print(f'[OS][X] uw-argtest failed - arg {num} should be {arg} but was {arg_str}')
                    all_args_correct = False

            if not all_args_correct:
                all_runs_correct = False
                print(' '.join(['[OS][X] uw-argtest failed - input was: p uw-testbin/argtest'] + args))
                if settings.STRICT_TESTS:
                    exit(1)
            
        except TimeoutExpired:
            print('[OS][X] uw-argtest failed - Timeout exceeded!')
            if settings.STRICT_TESTS:
                exit(1)
        except AssertionError:
            print(argtest_decoded)
            print('[OS][X] uw-argtest failed - Program did not execute correctly!')
            if settings.STRICT_TESTS:
                exit(1)

    if all_runs_correct:
        print('[OS][ ] uw-argtest PASSED')


def test_add():
    add_inputs = [
        (0, 0),
        (0, 1),
        (1, 1),
        (2, 3),
        (23, 53),
        (3611, 	9222),
        (7008, 	1403),
        (442,   786),
        (1510, 	7340),
        (4618, 	687),
        (8410, 	5983),
        (6925, 	3941),
        (1436, 	9253),
        (515,   1454),
        (7320, 	3025),
        (3149, 	6001),
        (1302, 	1482),
        (7369, 	6665),
        (6331, 	3199),
        (9972, 	3650),
        (366,   1662),
        (8648, 	1788),
        (8449, 	8857),
        (4211, 	5811),
        (1639, 	630),
        (6793, 	6677),
        (5944, 	6904),
        (6397, 	928),
        (7385, 	2862),
        (877,   8292),
        (4458, 	102),
        (9449, 	419),
        (1167, 	1156),
        (8143, 	8190),
        (5419, 	9481),
        (2337, 	7034),
        (7148, 	3375),
        (8635, 	6461),
        (4759, 	3823),
        (6358, 	3481),
        (8638, 	9713),
        (8418, 	5260),
        (6523, 	7920),
        (7212, 	2993),
        (1589, 	1142),
        (6809, 	2531),
        (6995, 	2144),
        (6582, 	485),
        (6464, 	8449),
        (1650, 	7507),
        (2822, 	1926),
        (2666, 	6981),
        (360,   3413),
        (7132, 	8567),
        (727,   177),
    ]

    all_adds_passed = True
    for x, y, in add_inputs:
        try:
            run_add = run(['sys161', 'kernel', f'p testbin/add {x} {y};q'],
                    cwd=CS350_ROOT, capture_output=True, timeout=5)
            run_add_decoded = run_add.stdout.decode('utf-8')
            output_str = re.search(r'Answer: (\d*)', run_add_decoded)
            assert output_str is not None

            if int(output_str.group(1)) == x+y:
                # Good
                pass
            else:
                all_adds_passed = False
                if settings.STRICT_TESTS:
                    print(run_add_decoded)
                    print('[OS][X] add failed - Incorrect output!')
                    exit(1)
            
        except TimeoutExpired:
            print('[OS][X] add failed - Timeout exceeded!')
            if settings.STRICT_TESTS:
                exit(1)
        except AssertionError:
            print(run_add_decoded)
            print('[OS][X] add failed - Program did not execute correctly!')
            if settings.STRICT_TESTS:
                exit(1)

    if all_adds_passed:
        print(f'[OS][ ] add PASSED (ran {len(add_inputs)} combinations)')

def test_all_execv_tests(which_tests: List[str]):
    if which_tests == []:
        which_tests = [all_tests]

    # Check for invalid params
    any_bad = False
    for test in which_tests:
        if test not in {all_tests, no_arg_passing_tests, 'hogparty', 'sty', 'argtesttest', 'argtest', 'uw-argtest', 'add'}:
            print(f'[OS][X] Got bad argument to test execv: {test}')
            any_bad = True

    if any_bad and settings.STRICT_TESTS:
        exit(1)

    set_mainboard_cfg(ram_size=2097152)
    for num_cpus in SIM_NUM_CPUS:
        print(f'[OS][ ] Running execv test {which_tests} with {num_cpus} CPUs')
        set_mainboard_cfg(num_cpus=num_cpus)
        if all_tests in which_tests or 'hogparty' in which_tests or no_arg_passing_tests in which_tests:
            test_hogparty()
        if all_tests in which_tests or 'sty' in which_tests or no_arg_passing_tests in which_tests:
            test_sty()
        if all_tests in which_tests or 'argtesttest' in which_tests:
            test_argtesttest()
        if all_tests in which_tests or 'argtest' in which_tests:
            test_argtest()
        if all_tests in which_tests or 'uw-argtest' in which_tests:
            test_uw_argtest()
        if all_tests in which_tests or 'add' in which_tests:
            test_add()

    # Reset to defaults
    set_mainboard_cfg(num_cpus=1, ram_size=524288)
