from collections import defaultdict
from multiprocessing import Pool
import re
from subprocess import run, TimeoutExpired
from time import time_ns
from typing import List

from kpptr.stats.operation_time_stats import OperationTimeStats
from kpptr import settings
from kpptr.settings import CS350_ROOT, NUM_CPUS, NUM_LOOPS, SIM_NUM_CPUS
from kpptr.utils import set_mainboard_cfg

all_tests = '__ALL__'

def test_forktest(which_tests):
    forktest_correct_output = {
        '0': 2,
        '1': 4,
        '2': 8,
        '3': 16,
    }

    if all_tests in which_tests or 'forktest' in which_tests:
        try:
            forktest = run(['sys161', 'kernel', 'p testbin/forktest;q'],
                    cwd=CS350_ROOT, capture_output=True, timeout=5)
            forktest_decoded = forktest.stdout.decode('utf-8')

            output_str = re.search(r'.*Starting\.(.*)Operation took.*', forktest_decoded, re.DOTALL)
            assert output_str is not None
            output_str = output_str.group(1)

            output = defaultdict(lambda: 0)
            for letter in output_str:
                output[letter] += 1

            all_correct = True
            for key in forktest_correct_output.keys():
                if forktest_correct_output[key] != output[key]:
                    print(f'[OS][X] forktest failed - Got {output[key]} of {key} but expected {forktest_correct_output[key]}')
                    all_correct = False

            if all_correct:
                print('[OS][ ] forktest PASSED')
            elif settings.STRICT_TESTS:
                exit(1)

        except TimeoutExpired:
            print('[OS][X] forktest failed - Timeout exceeded!')
            if settings.STRICT_TESTS:
                exit(1)
        except AssertionError:
            print(forktest_decoded)
            print('[OS][X] forktest failed - Program did not execute correctly!')
            if settings.STRICT_TESTS:
                exit(1)

    if all_tests in which_tests or 'forktest-w' in which_tests:
        try:
            forktest_wait = run(['sys161', 'kernel', 'p testbin/forktest -w;q'],
                    cwd=CS350_ROOT, capture_output=True, timeout=5)
            forktest_wait_decoded = forktest_wait.stdout.decode('utf-8')
            
            output_str = re.search(r'.*Starting\.(.*)Operation took.*', forktest_wait_decoded, re.DOTALL)
            assert output_str is not None
            output_str = output_str.group(1)

            output = defaultdict(lambda: 0)
            for letter in output_str:
                output[letter] += 1

            all_correct = True
            for key in forktest_correct_output.keys():
                if forktest_correct_output[key] != output[key]:
                    print(f'[OS][X] forktest -w failed - Got {output[key]} of {key} but expected {forktest_correct_output[key]}')
                    all_correct = False

            if all_correct:
                print('[OS][ ] forktest -w PASSED')
            elif settings.STRICT_TESTS:
                exit(1)

        except TimeoutExpired:
            print('[OS][X] forktest -w failed - Timeout exceeded!')
            if settings.STRICT_TESTS:
                exit(1)
        except AssertionError:
            print(forktest_wait_decoded)
            print('[OS][X] forktest -w failed - Program did not execute correctly!')
            if settings.STRICT_TESTS:
                exit(1)
        

def test_pidcheck():
    try:
        pidcheck = run(['sys161', 'kernel', 'p uw-testbin/pidcheck;q'],
                cwd=CS350_ROOT, capture_output=True, timeout=5)
        pidcheck_decoded = pidcheck.stdout.decode('utf-8')

        child_self_num = re.search(r'\nC: (\d*)', pidcheck_decoded)
        assert child_self_num is not None
        child_self_num = child_self_num.group(1)
        child_inspect_num = re.search(r'PC: (\d*)', pidcheck_decoded)
        assert child_inspect_num is not None
        child_inspect_num = child_inspect_num.group(1)
        parent_inspect_num = re.search(r'PP: (\d*)', pidcheck_decoded)
        assert parent_inspect_num is not None
        parent_inspect_num = parent_inspect_num.group(1)

        if parent_inspect_num != child_inspect_num and child_inspect_num == child_self_num:
            print('[OS][ ] pidcheck PASSED')
        else:
            print(pidcheck_decoded)
            print('[OS][X] pidcheck failed - Incorrect output! '
                    f'Parent num: {parent_inspect_num} '
                    f'Child Inspected Num: {child_inspect_num} '
                    f'Child Self Num: {child_self_num}')
            if settings.STRICT_TESTS:
                exit(1)
    except TimeoutExpired:
        print('[OS][X] pidcheck failed - Timeout exceeded!')
        if settings.STRICT_TESTS:
            exit(1)
    except AssertionError:
        print(pidcheck_decoded)
        print('[OS][X] pidcheck failed - Program did not execute correctly!')
        if settings.STRICT_TESTS:
            exit(1)
        

def test_onefork():
    onefork_correct_output = {
        'P': 1,
        'C': 1,
    }

    try:
        onefork = run(['sys161', 'kernel', 'p uw-testbin/onefork;q'],
                cwd=CS350_ROOT, capture_output=True, timeout=5)
        onefork_decoded = onefork.stdout.decode('utf-8')
        
        output_str = re.search(r'.*p uw-testbin\/onefork(.*)Operation took.*', onefork_decoded, re.DOTALL)
        assert output_str is not None
        output_str = output_str.group(1)

        output = defaultdict(lambda: 0)
        for letter in output_str:
            output[letter] += 1

        all_correct = True
        for key in onefork_correct_output.keys():
            if onefork_correct_output[key] != output[key]:
                print(f'[OS][X] onefork failed - Got {output[key]} of {key} but expected {onefork_correct_output[key]}')
                all_correct = False

        if all_correct:
            print('[OS][ ] onefork PASSED')
        elif settings.STRICT_TESTS:
            exit(1)

    except TimeoutExpired:
        print('[OS][X] onefork failed - Timeout exceeded!')
        if settings.STRICT_TESTS:
            exit(1)
    except AssertionError:
        print(onefork_decoded)
        print('[OS][X] onefork failed - Program did not execute correctly!')
        if settings.STRICT_TESTS:
            exit(1)
        

def test_widefork():
    widefork_correct_output = {
        'P': 3,
        'A': 1,
        'B': 1,
        'C': 1,
        'a': 1,
        'b': 1,
        'c': 1,
        'x': 0,
    }

    try:
        widefork = run(['sys161', 'kernel', 'p uw-testbin/widefork;q'],
                cwd=CS350_ROOT, capture_output=True, timeout=5)
        widefork_decoded = widefork.stdout.decode('utf-8')
        output_str = re.search(r'.*p uw-testbin\/widefork(.*)Operation took.*', widefork_decoded, re.DOTALL)
        assert output_str is not None
        output_str = output_str.group(1)

        output = defaultdict(lambda: 0)
        for letter in output_str:
            output[letter] += 1

        all_correct = True
        for key in widefork_correct_output.keys():
            if widefork_correct_output[key] != output[key]:
                print(f'[OS][X] widefork failed - Got {output[key]} of {key} but expected {widefork_correct_output[key]}')
                all_correct = False

        if all_correct:
            print('[OS][ ] widefork PASSED')
        elif settings.STRICT_TESTS:
            exit(1)
        
    except TimeoutExpired:
        print('[OS][X] widefork failed - Timeout exceeded!')
        if settings.STRICT_TESTS:
            exit(1)
    except AssertionError:
        print(widefork_decoded)
        print('[OS][X] widefork failed - Program did not execute correctly!')
        if settings.STRICT_TESTS:
            exit(1)

def test_all_fork_tests(which_tests: List[str]):
    if which_tests == []:
        which_tests = [all_tests]

    # Check for invalid params
    any_bad = False
    for test in which_tests:
        if test not in {all_tests, 'onefork', 'pidcheck', 'forktest', 'forktest-w', 'widefork'}:
            print(f'[OS][X] Got bad argument to test fork: {test}')
            any_bad = True

    if any_bad and settings.STRICT_TESTS:
        exit(1)

    set_mainboard_cfg(ram_size=4194304)
    for num_cpus in SIM_NUM_CPUS:
        print(f'[OS][ ] Running fork test {which_tests} with {num_cpus} CPUs')
        set_mainboard_cfg(num_cpus=num_cpus)
        if all_tests in which_tests or 'onefork' in which_tests:
            test_onefork()
        if all_tests in which_tests or 'pidcheck' in which_tests:
            test_pidcheck()
        if all_tests in which_tests or 'forktest' in which_tests or 'forktest-w' in which_tests:
            test_forktest(which_tests)
        if all_tests in which_tests or 'widefork' in which_tests:
            test_widefork()

    # Reset to defaults
    set_mainboard_cfg(num_cpus=1, ram_size=524288)