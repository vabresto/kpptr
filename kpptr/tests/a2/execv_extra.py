from collections import defaultdict
import re
from subprocess import run, TimeoutExpired
from time import time_ns
from typing import List

from kpptr.stats.operation_time_stats import OperationTimeStats
from kpptr import settings
from kpptr.settings import CS350_ROOT, NUM_CPUS, NUM_LOOPS, SIM_NUM_CPUS
from kpptr.utils import set_mainboard_cfg

all_tests = '__ALL__'

def test_badcall_execv():
    try:
        badcall = run(['sys161', 'kernel', 'p testbin/badcall a;q'],
                cwd=CS350_ROOT, capture_output=True, timeout=5)
        badcall_decoded = badcall.stdout.decode('utf-8')
        output_str = re.search(r'.*p testbin\/badcall a(.*)Operation took.*', badcall_decoded, re.DOTALL)
        assert output_str is not None
        print('[OS][ ] badcall execv PASSED')
        
    except TimeoutExpired:
        print('[OS][X] badcall execv failed - Timeout exceeded!')
        if settings.STRICT_TESTS:
            exit(1)
    except AssertionError:
        print(badcall_decoded)
        print('[OS][X] badcall execv failed - Program did not execute correctly!')
        if settings.STRICT_TESTS:
            exit(1)

def test_all_execv_extra_tests(which_tests: List[str]):
    if which_tests == []:
        which_tests = [all_tests]

    # Check for invalid params
    any_bad = False
    for test in which_tests:
        if test not in {all_tests}:
            print(f'[OS][X] Got bad argument to test execv extra: {test}')
            any_bad = True

    if any_bad and settings.STRICT_TESTS:
        exit(1)

    set_mainboard_cfg(ram_size=4194304)
    for num_cpus in SIM_NUM_CPUS:
        print(f'[OS][ ] Running execv extra test {which_tests} with {num_cpus} CPUs')
        set_mainboard_cfg(num_cpus=num_cpus)

        test_badcall_execv()
        
    # Reset to defaults
    set_mainboard_cfg(num_cpus=1, ram_size=524288)
