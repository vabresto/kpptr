from collections import defaultdict
from multiprocessing import Pool
import re
from subprocess import run, TimeoutExpired
from time import time_ns
from typing import List

from kpptr import settings
from kpptr.settings import CS350_ROOT, NUM_CPUS, NUM_LOOPS, SIM_NUM_CPUS
from kpptr.utils import set_mainboard_cfg

def test_vm_mix1_fork():
    for mem_size in [524288, 4194304, 16777216]:
        for num_cpus in SIM_NUM_CPUS:
            set_mainboard_cfg(num_cpus=num_cpus, ram_size=mem_size)
            print(f'[OS][ ] Running uw-testbin/vm-mix1-fork with {num_cpus} CPUs and {mem_size} memory')
            try:
                vm_mix1_fork = run(['sys161', 'kernel', 'p uw-testbin/vm-mix1-fork;q'],
                        cwd=CS350_ROOT, capture_output=True, timeout=10)
                vm_mix1_fork_decoded = vm_mix1_fork.stdout.decode('utf-8')
                
                if 'dumbvm: Ran out of TLB entries - cannot handle page fault' in vm_mix1_fork_decoded:
                    print('[OS][X] vm_mix1_fork partially failed - Ran out of TLB entries')
                    if settings.STRICT_TESTS:
                        exit(1)
                    else:
                        continue

                if 'Unable to fork' in vm_mix1_fork_decoded:
                    print('[OS][X] vm_mix1_fork failed - Unable to fork')
                    if settings.STRICT_TESTS:
                        exit(1)
                    else:
                        continue

                if 'Out of memory' in vm_mix1_fork_decoded:
                    print('[OS][X] vm_mix1_fork failed - Out of memory')
                    if settings.STRICT_TESTS:
                        exit(1)
                    else:
                        continue

                output_str = re.search(r'.*p uw-testbin\/vm-mix1-fork(.*)Operation took.*', vm_mix1_fork_decoded, re.DOTALL)
                assert output_str is not None
                output_str = output_str.group(1)

                if 'Pid = 1 SUCCEEDED' in vm_mix1_fork_decoded and 'Pid = 2 SUCCEEDED' in vm_mix1_fork_decoded:
                    print('[OS][ ] vm_mix1_fork PASSED')
                else:
                    if not settings.STRICT_TESTS and vm_mix1_fork_decoded.count('SUCCEEDED') == 2:
                        print('[OS][~] vm_mix1_fork probably passed')
                    else:
                        print(vm_mix1_fork_decoded)
                        print('[OS][X] vm_mix1_fork probably failed (?) - could not find success condition')
                        if settings.STRICT_TESTS:
                            exit(1)

            except TimeoutExpired:
                print('[OS][X] vm_mix1_fork failed - Timeout exceeded!')
                if settings.STRICT_TESTS:
                    exit(1)
            except AssertionError:
                print(vm_mix1_fork_decoded)
                print('[OS][X] vm_mix1_fork failed - Program did not execute correctly!')
                if settings.STRICT_TESTS:
                    exit(1)
