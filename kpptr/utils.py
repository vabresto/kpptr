#! /usr/bin/python3.7
import re
from typing import Optional

from kpptr.settings import CS350_ROOT


def get_operation_time(log: str) -> str:
    time = re.search(r'Operation took ([\d\.]*) seconds', log)
    if time is None:
        return '???'
    return time.group(1)


def set_mainboard_cfg(num_cpus: Optional[int] = None, ram_size: Optional[int] = None) -> None:
    # Change the settings file
    with open(CS350_ROOT / 'sys161.conf', 'r+') as rwfh:
        conf = rwfh.read()
        cur_settings = re.search(r'#?\s*31\s*mainboard\s*ramsize=(\d*)\s*cpus=(\d*)', conf)

        if num_cpus is None:
            num_cpus = int(cur_settings.group(2))
        
        assert 1 <= num_cpus <= 8, f"num_cpus must be between 1 and 8, inclusive but got {num_cpus}"

        if ram_size is None:
            ram_size = int(cur_settings.group(1))
        
        assert ram_size % 4096 == 0, f"ram_size must be divisible by 4096 but got {ram_size}!"

        conf = re.sub(
            r'#?\s*31\s*mainboard\s*ramsize=(\d*)\s*cpus=(\d*)', '', conf)            
        conf += f'31 mainboard ramsize={ram_size} cpus={num_cpus}\n'

        rwfh.seek(0)
        rwfh.truncate()
        rwfh.write(conf)
