#! /usr/bin/python3.7
from datetime import datetime
import hashlib
from pathlib import Path
import re
from typing import List, Optional, Tuple
import shutil
from subprocess import run, CalledProcessError

from kpptr.settings import CS350_OS161, CS350_ROOT, CS350_HOME, KPPTR_HOME


def run_kernel(version: int, commands: Optional[List[str]]) -> None:
    if commands is None:
        commands = []

    commands += ';q'

    if version == -1:
        # Use latest version
        run(['sys161', 'kernel'] + commands, cwd=CS350_ROOT)
    else:
        run(['sys161', f'kernel-ASST{version}'] + commands, cwd=CS350_ROOT)


def wait_debug_kernel(version: int, commands: Optional[List[str]]) -> None:
    if commands is None:
        commands = []

    commands += ';q'

    if version == -1:
        # Use latest version
        run(['sys161', '-w', 'kernel'] + commands, cwd=CS350_ROOT)
    else:
        run(['sys161', '-w', f'kernel-ASST{version}'] + commands, cwd=CS350_ROOT)


def gdb_kernel(version: int, commands: Optional[List[str]]) -> None:
    hook_commands = []

    if commands is None:
        commands = []
    else:
        commands = [val for pair in zip(['-ex'] * len(commands), commands) for val in pair]

    hook_commands.append('-ex')
    hook_commands.append(f'dir ../os161-1.99/kern/compile/ASST{version}')

    hook_commands.append('-ex')
    hook_commands.append('target remote unix:.sockets/gdb')

    run(['cs350-gdb'] + hook_commands + commands + ['kernel'], cwd=CS350_ROOT)


def compile_kernel(version: int) -> None:
    work_dir = CS350_OS161 / 'kern' / 'compile' / f'ASST{version}'
    depend = run(['bmake', 'depend'],
                 cwd=work_dir,
                 capture_output=True)
    try:
        depend.check_returncode()
    except CalledProcessError:
        print(depend.stderr.decode('utf-8'))
        exit(depend.returncode)

    bmake = run(['bmake'],
                cwd=work_dir,
                capture_output=True)
    try:
        bmake.check_returncode()
    except CalledProcessError:
        print(bmake.stderr.decode('utf-8'))
        exit(bmake.returncode)

    install = run(['bmake', 'install'],
                  cwd=work_dir,
                  capture_output=True)

    try:
        install.check_returncode()
    except CalledProcessError:
        print(install.stderr.decode('utf-8'))
        exit(install.returncode)

    build_version = re.search(
        rf'This is ASST{version} build #(\d*)', bmake.stdout.decode('utf-8'))
    if build_version is None:
        print('[OS][ ] Kernel was NOT recompiled as there were no changes made')
    else:
        print(
            f'[OS][ ] Successfully compiled and installed ASST{version} build #{build_version.group(1)}')

def submit_kernel(version: str) -> None:
    source_file = CS350_HOME / 'os161kern.tgz'
    dest_folder = KPPTR_HOME / 'submissions' / version
    valid_assignments = ['0', '1', '2a', '2b', '3']

    if version not in valid_assignments:
        print('[OS][X] Unable to submit assignment! '
                f'Assignment version must be one of {valid_assignments}.\n'
                'For example, ./os --submit 0 or ./os --submit 2a')
        exit(1)

    print(f'[OS][ ] Submitting {version} ...')

    # Delete file if it exists already
    try:
        source_file.unlink()
    except FileNotFoundError:
        pass

    dest_folder.mkdir(parents=True, exist_ok=True)

    submit = run(['cs350_submit', version],
                cwd=CS350_HOME,
                capture_output=True)
    submit_decoded = submit.stdout.decode('utf-8')

    # Hash and save what we submitted
    with open(source_file,"rb") as file_name:
        # https://stackoverflow.com/a/20415341
        readable_hash = hashlib.sha512(file_name.read()[8:]).hexdigest()

    if 'submission is complete!' in submit_decoded:
        with open(dest_folder / 'history.log', 'a') as wfh:
            wfh.write(f'Submitted {readable_hash} at {datetime.now()}\n')

        shutil.move(source_file, dest_folder / f'os161kern-{readable_hash}.tgz')
        print(f'[OS][ ] Submitted os161 with SHA-512 hash {readable_hash} for assignment {version}')
    else:
        print(f'[OS][X] Submission failed!')
        print('---')
        print(submit_decoded)