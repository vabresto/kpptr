#! /usr/bin/python3.7
from argparse import ArgumentParser

def build_parser() -> ArgumentParser:
    parser = ArgumentParser(description='Utility program for CS 350')

    parser.add_argument('-r', '--run', const=-1, type=int, nargs='?', required=False,
                        help='Run the specified kernel version number (defaults to latest) (ex. --run 1)')

    parser.add_argument('-a', '--arg', type=str, nargs='*', required=False,
                        help='Pass the following commands to the kernel (will automatically quit after the commands) (ex. --arg "sy2")')

    parser.add_argument('-c', '--compile', type=int, required=False,
                        help='Compile the specified kernel version (ex. --compile 1)')

    parser.add_argument('-s', '--submit', type=str, required=False,
                        help='Submit the specified kernel version (ex. --submit 1)')

    parser.add_argument('-w', '--wait', const=-1, type=int, nargs='?', required=False,
                        help='Start the kernel but wait for the debugger (same argument as --run)')

    parser.add_argument('-d', '--debug', type=int, required=False,
                        help='Start the debugger (same argument as --run)(DOES NOT HAVE A DEFAULT)')

    parser.add_argument('-tl', '--test-locks', action='store_true', default=False, required=False,
                        help='Run lock tests a statistically significant number of times')

    parser.add_argument('-tcv', '--test-cvs', action='store_true', default=False, required=False,
                        help='Run conditional variable tests a statistically significant number of times')

    parser.add_argument('-tt', '--test-traffic', action='store_true', default=False, required=False,
                        help='Run A1 traffic tests a statistically significant number of times (warnings issued at exceeding 90 percent of time budget)')

    parser.add_argument('-ttr', '--test-traffic-random', const=-1, type=int, nargs='?', required=False,
                        help='Run A1 traffic tests with a seed (leave blank to use a random seed)')

    parser.add_argument('-tf', '--test-fork', nargs='*', type=str, default=None, required=False,
                        help='Run fork tests. Runs all by default. (forktest, forktest-w, pidcheck, onefork, widefork) (ex. --test-fork onefork pidcheck)')

    parser.add_argument('-tfx', '--test-fork-extra', action='store_true', default=False, required=False,
                        help='Run some extra fork tests (currently only runs "p uw-testbin/vm-mix1-fork")')

    parser.add_argument('-tx', '--test-execv', nargs='*', type=str, default=None, required=False,
                        help='Run execv and runprogram tests. Runs all by default. (hogparty, sty, argtesttest, argtest, uw-argtest, add, __NO_ARG_PASSING__)')

    parser.add_argument('-txx', '--test-execv-extra', nargs='*', type=str, default=None, required=False,
                        help='Run extra tests for execv (currently only runs "p testbin/badcall a")')

    parser.add_argument('-tvm', '--test-virtual-memory', nargs='*', type=str, default=None, required=False,
                        help='Run virtual memory tests. Runs all by default. (vm-data1, vm-data3, romemwrite, vm-crash2, sort, matmult, vm-data1-rep, vm-hogparty, vm-widefork)')

    parser.add_argument('--one-cpu', action='store_true', default=False, required=False,
                        help='Reset the number of CPUs to use to 1')

    parser.add_argument('--set-cpus', type=int, required=False,
                        help="Set the number of CPUs to use in OS161 (NB: Will persist between OS runs!)")

    parser.add_argument('--set-mem', type=int, required=False,
                        help="Set the RAM size to use in OS161. Must be a multiple of 4096. (Original value is 524288) (NB: Will persist between OS runs!)")

    parser.add_argument('--strict', action='store_true', default=False, required=False,
                        help='Strictly adhere to test guidelines; will exit on first failure (default=false)')

    parser.add_argument('-u', '--update', action='store_true', default=False, required=False,
                        help='Update Kernel Puppeteer to the latest available version')

    parser.add_argument('-cu', '--check-updates', action='store_true', default=False, required=False,
                        help='Check if there are any updates for Kernel Puppeteer. No changes will be made locally.')

    return parser