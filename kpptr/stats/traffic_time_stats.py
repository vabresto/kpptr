#! /usr/bin/python3.7
from __future__ import annotations

from dataclasses import dataclass

@dataclass
class TrafficTimeStats:
    @dataclass
    class TrafficDirectionStats:
        _avg_wait: Optional[float] = None
        _max_wait: Optional[float] = None
        _num_cars: Optional[float] = None

        @classmethod
        def from_logs(cls, match: re.Match):
            return cls(_avg_wait=float(match.group(2)),
                        _max_wait=float(match.group(3)),
                        _num_cars=float(match.group(1)))

        @classmethod
        def merge(cls, a: TrafficTimeStats.TrafficDirectionStats, b: TrafficTimeStats.TrafficDirectionStats):
            if a._num_cars is None:
                return b
            if b._num_cars is None:
                return a

            assert a._avg_wait is not None
            assert a._max_wait is not None
            assert b._avg_wait is not None
            assert b._max_wait is not None

            total_cars = (a._num_cars + b._num_cars)

            return cls(_avg_wait=(a._avg_wait / total_cars * a._num_cars) + (b._avg_wait / total_cars * b._num_cars),
                        _max_wait=max(a._max_wait, b._max_wait),
                        _num_cars=total_cars)
        
        def __str__(self):
            return ('{'
            f'cars: {self._num_cars}, avg: {self._avg_wait}, max: {self._max_wait}'
            '}')

    _max_allowed: Optional[float] = None
    _north: TrafficDirectionStats = TrafficDirectionStats()
    _east: TrafficDirectionStats = TrafficDirectionStats()
    _south: TrafficDirectionStats = TrafficDirectionStats()
    _west: TrafficDirectionStats = TrafficDirectionStats()

    @classmethod
    def merge(cls, a: TrafficTimeStats, b: TrafficTimeStats):
        if a._max_allowed is None:
            return b
        if b._max_allowed is None:
            return a
        if a._max_allowed != b._max_allowed:
            raise RuntimeError(f'Cannot merge traffic stats! a\'s max allowed ({a._max_allowed}) does not match b\'s ({b._max_allowed})!')

        return cls(_max_allowed=a._max_allowed,
                    _north=cls.TrafficDirectionStats.merge(a._north, b._north),
                    _east=cls.TrafficDirectionStats.merge(a._east, b._east),
                    _south=cls.TrafficDirectionStats.merge(a._south, b._south),
                    _west=cls.TrafficDirectionStats.merge(a._west, b._west),)

    def max_wait_incurred(self):
        return max(self._north._max_wait, self._east._max_wait, self._south._max_wait, self._west._max_wait)

    def imbalance_of_averages(self):
        max_avg = max(self._north._avg_wait, self._east._avg_wait, self._south._avg_wait, self._west._avg_wait)
        min_avg = min(self._north._avg_wait, self._east._avg_wait, self._south._avg_wait, self._west._avg_wait)
        return max_avg - min_avg

    def __str__(self):
        return ('['
        f'N: {self._north}; '
        f'E: {self._east}; '
        f'S: {self._south}; '
        f'W: {self._west} '
        ']')
