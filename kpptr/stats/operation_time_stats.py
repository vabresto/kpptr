#! /usr/bin/python3.7
from __future__ import annotations

from dataclasses import dataclass

@dataclass
class OperationTimeStats:
    _min: Optional[float] = None
    _max: Optional[float] = None
    _avg: Optional[float] = None
    _num_data = 0

    @classmethod
    def merge(cls, a: 'OperationTimeStats', b: 'OperationTimeStats') -> 'OperationTimeStats':
        ret = OperationTimeStats()
        if a._num_data == 0:
            return b
        if b._num_data == 0:
            return a

        assert a._min is not None
        assert a._max is not None
        assert a._avg is not None

        assert b._min is not None
        assert b._max is not None
        assert b._avg is not None

        ret._min = min(a._min, b._min)
        ret._max = max(a._max, b._max)
        ret._num_data = a._num_data + b._num_data
        ret._avg = (a._avg * a._num_data + b._avg *
                    b._num_data) / (ret._num_data)
        return ret

    def add_stat(self, time: float):
        if self._min is None:
            self._min = time
        else:
            self._min = min(self._min, time)

        if self._max is None:
            self._max = time
        else:
            self._max = max(self._max, time)

        if self._avg is None:
            self._avg = time
            self._num_data = 1
        else:
            self._avg = (time + self._avg * self._num_data) / \
                (self._num_data + 1)
            self._num_data += 1

    def __str__(self):
        return ('{'
                f'min: {self._min},  avg: {self._avg}, max: {self._max}'
                '}')
