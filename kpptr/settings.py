#! /usr/bin/python3.7
from pathlib import Path

KPPTR_HOME = Path.home() / '.kpptr'
CS350_HOME = Path.home() / 'cs350-os161'
CS350_ROOT = CS350_HOME / 'root'
CS350_OS161 = CS350_HOME / 'os161-1.99'
NUM_LOOPS = 100
NUM_CPUS = 5
SIM_NUM_CPUS = [1, 2, 4, 8]
STRICT_TESTS = False