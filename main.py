#! /usr/bin/python3.7

# Add ourselves to python path
from pathlib import Path
import os
import sys
sys.path.append(str(Path(os.path.realpath(__file__)).parent.resolve()))

from kpptr.parser.parser import build_parser
from kpptr.parser.parser_actions import compile_kernel, gdb_kernel, run_kernel, wait_debug_kernel, submit_kernel
from kpptr import settings
from kpptr.tests import check_and_run_tests
from kpptr.update import check_for_updates, run_update
from kpptr.utils import set_mainboard_cfg

def main():
    parser = build_parser()
    args = parser.parse_args()

    if args.check_updates is not None and args.check_updates:
        check_for_updates()

    if args.update is not None and args.update:
        run_update()

    if args.strict is not None and args.strict:
        settings.STRICT_TESTS = True

    if args.compile is not None:
        compile_kernel(args.compile)

    if args.one_cpu is not None and args.one_cpu:
        set_mainboard_cfg(num_cpus=1)

    if args.set_cpus is not None or args.set_mem is not None:
        set_mainboard_cfg(num_cpus=args.set_cpus, ram_size=args.set_mem)

    if args.debug is not None:
        gdb_kernel(args.debug, args.arg)

    if args.run is not None:
        run_kernel(args.run, args.arg)
    elif args.wait is not None:
        wait_debug_kernel(args.wait, args.arg)

    check_and_run_tests(args)

    if args.submit is not None:
        submit_kernel(args.submit)


if __name__ == '__main__':
    main()